require 'spec_helper'

describe "Simple Test", {:sauce=>true} do
  
  it "should go to Google" do 
    selenium.navigate.to "http://www.saucelabs.com"
    expect(selenium.driver.title).to include("Sauce Labs")
  end
end