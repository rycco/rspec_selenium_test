# You should edit this file with the browsers you wish to use
# For options, check out http://saucelabs.com/docs/platforms
require "sauce"

Sauce.config do |config|
  config[:browsers] = [
    ["OS X 10.10", "iPhone", "8.1"]

  ]
  config[:start_tunnel] = false
  config[:start_local_application] = false

end
